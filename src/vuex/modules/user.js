import axios from '../../axios/http'
import router from '../../router/index'
import {Message} from 'element-ui';
const user = {
  state: {
    name: '',
    adminId: 0,
    companyId: 0,
    menus: [],
    status: 'login',
    phone: '',
    companyName: '后台管理系统',
    activexObj: null,
    againRegist: false,
    plamStep: 0,
    userId: 0,
    registStatus: {
      index: 0,
      handTxt: '请把手掌放置正确位置'
    },
    isOpen: false,
    registResult: {
      status: 0,
      value: ''
    },
    equipmentId: 1,
    isUpdate: false
  },
  mutations: {
    userInfo(state, payload) {
      let data = payload.data;
      state.adminId = data.id === undefined ? 0: data.id;
      state.companyId = data.companyId === undefined ? 0 : data.companyId;
      state.companyName = data.companyName === null ? '后台管理系统' : data.companyName;
      state.name = data.name === undefined ? '' : data.name;
      state.phone = data.phone === undefined ? '' : data.phone;
    },
    menuList(state, payload) {
      state.menus = payload.menus
    },
    activexObj(state, payload) {
      state.activexObj = payload.activexObj
    },
    startRegist(state, payload) {
      state.userId = payload.userId;
      state.isOpen = true;
      state.activexObj.capturePalmData();
      state.plamStep = 1;
    },
    capturePalmData(state, payload) {
      state.plamStep = 0;
      let veinData = payload.veinData;
      axios.post('/excluded/vein/find.do', {
        vein: veinData,
        equipmentName: state.equipmentId
      }).then((result) => {
        if (parseInt(result.result) === 0) {
          //验证完成掌静脉已存在
          state.plamStep = 0;
          state.registResult.status = 2;
          state.registResult.value = '您的掌静脉已存在';
        } else {
          state.activexObj.enrollBtnClick(); //开始注册
          state.plamStep = 2;
        }
      }).catch((error) => {
        Message.error(error);
      })
    },
    enrollData(state, payload) {
      let registData = payload.registData;
      Message.success(registData)
      state.plamStep = 0;
      axios.post('/userInfo/ActiveVein.do', {
        vein: registData,
        adminId: state.adminId,
        userId: state.userId
      }).then((response) => {
         let result_data = response.data;
         if (parseInt(result_data.status) === 1) {
             //绑定成功
            state.registResult.status = 1;
         } else {
            state.registResult.status = 2;
         }
         state.registResult.value = result_data.msg;
         state.isUpdate = true;
      }).catch((error) => {
        Message.error(error);
      })
    },
    registStatus(state, payload) {
      //注册过程的状态信息
      state.registStatus = payload.registStatus;
    },
    closePlam(state, payload) {
      //取消注册
      state.isUpdate = false;
      state.isOpen = false;
      state.registResult.status = 0;
      state.registResult.value = '';
      state.registStatus.index = 0;
      state.registStatus.handTxt = '请把手掌放置正确位置';
      if (state.plamStep === 1) {
        state.activexObj.cancleCapturePalm(); //取消验证
      } else if (state.plamStep === 2) {
        state.activexObj.cancleEnroll(); //取消注册
      }
      state.plamStep = 0;
    },
    againRegistPalm (state, payload) {
      //重新注册
      if (state.plamStep === 1) {
        state.againRegist = true;
        state.activexObj.cancleCapturePalm();//取消验证
      } else if (state.plamStep === 2) {
        state.againRegist = true;
        state.activexObj.cancleEnroll();//取消注册
      } else {
        state.activexObj.capturePalmData(); //开启验证
        state.plamStep = 1;
      }
    },
    changeStep(state, payload) {
      let status = payload.resetStep;
      state.plamStep = status.plamStep;
      state.againRegist = status.againRegist;
    },
    cancleSoft(state, payload) {
      //取消注册
      if (state.plamStep === 1) {
          state.activexObj.cancleCapturePalm(); //取消验证
      } else if (state.plamStep === 2) {
          state.activexObj.cancleEnroll(); //取消注册
      }
      state.plamStep = 0;
    },
    cancleAllSoft (state, payload) {
      //返回
      state.activexObj.cancleEnroll(); //取消注册
      state.activexObj.cancleCapturePalm(); //取消验证
    },
    finishSoft (state, payload) {
      state.activexObj.Ps_Sample_Apl_CS_FinishLibrary(); //关闭掌静脉
    }
  },
  actions: {

  }
}

export default user