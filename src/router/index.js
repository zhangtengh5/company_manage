import Vue from 'vue';
import Router from 'vue-router';
import store from '../vuex/index'
import axios from '../axios/http'
Vue.use(Router);

let router = new Router({
    routes: [{
            path: '/',
            redirect: '/dashboard'
        },
        {
            path: '/',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: {
                title: ''
            },
            children: [{
                    path: '/dashboard',
                    component: resolve => require(['../components/page/Dashboard.vue'], resolve),
                    meta: {
                        title: '系统首页',
                        requiresAuth: true
                    }
                },
                {
                    path: '/employee',
                    component: resolve => require(['../components/page/Employee.vue'], resolve),
                    meta: {
                        title: '人员管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/role',
                    component: resolve => require(['../components/page/Role.vue'], resolve),
                    meta: {
                        title: '角色管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/equipment',
                    component: resolve => require(['../components/page/Equipment.vue'], resolve),
                    meta: {
                        title: '设备管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/passlog',
                    component: resolve => require(['../components/page/Passlog.vue'], resolve),
                    meta: {
                        title: '通行日志',
                        requiresAuth: true
                    }
                },
                {
                    path: '/deletelog',
                    component: resolve => require(['../components/page/Deletelog.vue'], resolve),
                    meta: {
                        title: '人员删除日志',
                        requiresAuth: true
                    }
                },
                {
                    path: '/cardlog',
                    component: resolve => require(['../components/page/Cardlog.vue'], resolve),
                    meta: {
                        title: '打卡记录',
                        requiresAuth: true
                    }
                },
                {
                    path: '/dolog',
                    component: resolve => require(['../components/page/Dolog.vue'], resolve),
                    meta: {
                        title: '操作日志',
                        requiresAuth: true
                    }
                }
            ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Login.vue'], resolve)
        },
        {
            path: '/forget',
            component: resolve => require(['../components/page/Forget.vue'], resolve)
        },

        {
            path: '/regist',
            component: resolve => require(['../components/page/Regist.vue'], resolve)
        },
        {
            path: '*',
            redirect: '/404'
        },
        {
            path: '/404',
            component: resolve => require(['../components/page/404.vue'], resolve)
        }
    ]
})

router.beforeEach((to, from, next) => {
    let data = window.sessionStorage.getItem('ztmj')
    if (data) {
        store.commit({
            type: 'userInfo',
            data: JSON.parse(data)
        })
    }
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let adminId = store.getters.adminId
        axios.post('/admin/GetMySession.do', {})
            .then((response) => {
                let result_data = response.data;
                if (parseInt(result_data.status) === 1) {
                    if (adminId) {
                        next()
                    } else {
                        next({
                            path: '/login'
                        })
                    }
                } else {
                    next({
                        path: '/login'
                    })
                }
            })
            .catch((error) => {
                next({
                    path: '/login'
                })
            })
    } else {
        next() // 确保一定要调用 next()
    }
})

export default router;
